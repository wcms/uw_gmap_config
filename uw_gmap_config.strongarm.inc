<?php

/**
 * @file
 * uw_gmap_config.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_gmap_config_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gmap_api_key';
  $strongarm->value = 'AIzaSyAQDR9fxCGliviHRs7Cd7IHcokMTk-2whI';
  $export['gmap_api_key'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gmap_default';
  $strongarm->value = array(
    'width' => '100%',
    'height' => '300px',
    'latlong' => '43.470487171491094,-80.54338932037354',
    'zoom' => '15',
    'maxzoom' => '16',
    'styles' => array(
      'line_default' => array(
        0 => '0000ff',
        1 => '5',
        2 => '45',
        3 => '',
        4 => '',
      ),
      'poly_default' => array(
        0 => '000000',
        1 => '3',
        2 => '25',
        3 => 'ff0000',
        4 => '45',
      ),
      'highlight_color' => 'ff0000',
    ),
    'controltype' => 'Small',
    'mtc' => 'standard',
    'maptype' => 'Map',
    'baselayers' => array(
      'Map' => 1,
      'Satellite' => 1,
      'Hybrid' => 1,
      'Physical' => 0,
    ),
    'behavior' => array(
      'locpick' => FALSE,
      'nodrag' => 0,
      'nokeyboard' => 0,
      'nomousezoom' => 1,
      'nocontzoom' => 0,
      'autozoom' => 0,
      'dynmarkers' => 0,
      'overview' => 0,
      'collapsehack' => 0,
      'scale' => 1,
      'extramarkerevents' => FALSE,
      'clickableshapes' => FALSE,
      'googlebar' => 0,
      'highlight' => 0,
    ),
    'markermode' => '0',
    'line_colors' => array(
      0 => '#00cc00',
      1 => '#ff0000',
      2 => '#0000ff',
    ),
  );
  $export['gmap_default'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gmap_markermanager';
  $strongarm->value = array(
    'markermanager' => array(
      'filename' => 'markermanager_packed.js',
      'borderPadding' => '256',
      'maxZoom' => '4',
      'trackMarkers' => 0,
      'markerMinZoom' => '4',
      'markerMaxZoom' => '0',
    ),
    'markerclusterer' => array(
      'filename' => 'markerclusterer_packed.js',
      'gridSize' => 60,
      'maxZoom' => 17,
      'styles' => array(),
    ),
    'clusterer' => array(
      'filename' => 'Clusterer2.js',
      'marker' => '',
      'max_nocluster' => '150',
      'cluster_min' => '5',
      'max_lines' => '10',
      'popup_mode' => 'orig',
    ),
    'clustermarker' => array(
      'filename' => 'ClusterMarker.js',
      'borderPadding' => '256',
      'clusteringEnabled' => 1,
      'clusterMarkerIcon' => '',
      'clusterMarkerTitle' => '',
      'fitMapMaxZoom' => '0',
      'intersectPadding' => '0',
    ),
  );
  $export['gmap_markermanager'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gmap_marker_custom_dir';
  $strongarm->value = 'profiles/uw_base_profile/modules/features/uw_gmap_config/markers';
  $export['gmap_marker_custom_dir'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gmap_mm_type';
  $strongarm->value = 'gmap';
  $export['gmap_mm_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gmap_node_map';
  $strongarm->value = array(
    'macro' => '[gmap |id=nodemap|center=40,0|zoom=3|width=100%|height=400px]',
    'header' => 'This map illustrates the locations of the nodes on this website. Each marker indicates a node associated with a specific location.',
    'footer' => '',
    'markermode' => '1',
  );
  $export['gmap_node_map'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gmap_node_markers';
  $strongarm->value = array(
    'uw_blog' => '',
    'contact' => '',
    'uwaterloo_custom_listing' => '',
    'uw_event' => '',
    'uw_home_page_banner' => '',
    'uw_image_gallery' => '',
    'uw_news_item' => '',
    'uw_ct_person_profile' => '',
    'uw_project' => '',
    'uw_promotional_item' => '',
    'uw_service' => '',
    'uw_special_alert' => '',
    'uw_web_form' => '',
    'uw_web_page' => '',
    'uw_site_footer' => '',
    'uw_student_award' => '',
    'webform' => '',
  );
  $export['gmap_node_markers'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gmap_role_markers';
  $strongarm->value = array(
    2 => '',
    3 => '',
    6 => '',
    5 => '',
    4 => '',
    7 => '',
    8 => '',
    9 => '',
    12 => '',
    11 => '',
  );
  $export['gmap_role_markers'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gmap_user_map';
  $strongarm->value = array(
    'macro' => '[gmap |id=usermap|center=40,0|zoom=3|width=100%|height=400px]',
    'header' => 'This map illustrates the extent of users of this website. Each marker indicates a user that has entered their locations.',
    'footer' => '',
    'markermode' => '1',
  );
  $export['gmap_user_map'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'googlemap_api_key';
  $strongarm->value = 'AIzaSyAQDR9fxCGliviHRs7Cd7IHcokMTk-2whI';
  $export['googlemap_api_key'] = $strongarm;

  return $export;
}
